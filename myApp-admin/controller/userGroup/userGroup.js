'use strict';
//读取文件
const AdminModel = require('../../models/admin/admin.js')
const AddressComponent = require('../../prototype/addressComponent')
const formidable = require('formidable')
const userGroupModel = require('../../models/admin/userGroup.js')
const userGroupMenuModel = require('../../models/menu/userGroup_menu.js')
const menuModel = require('../../models/menu/menu.js')
class userGroup extends AddressComponent {
    constructor() {
        super()
        this.index = this.index.bind(this)
        this.insert = this.insert.bind(this)
        this.update = this.update.bind(this)
        this.deletes = this.deletes.bind(this)
        this.read = this.read.bind(this)
        this.saveauth = this.saveauth.bind(this)
    }
    //导航菜单栏数据
    //只能看到自己权限能看到的
    async index(req, res, next) {
        //通过该方法创建一个form表单
        const form = new formidable.IncomingForm();
        form.parse(req, async(err, fields, files) => {
            if (err) {
                this.reBody(res)
                return
            }
            const { UserToken, is_menu } = fields;
            //验证token
            let xtoken = await this.settoken(req, UserToken);
            if (xtoken == 1) {
                //1007表示要重新登录
                this.reBody(res, 0, 1007, "无效请求参数", '无效请求参数！')
                return
            }
            try {
                //通过ID找到身份
                const userGroupData = await userGroupModel.find()
                this.reBody(res, 0, 1001, "无效请求参数", userGroupData)
            } catch (err) {
                this.reBody(res, 0, 1005, "无效请求参数", '无效请求参数！')
            }
        })
    }
    //查看当前用户组的权限
    async read(req, res, next) {
        //通过该方法创建一个form表单
        const form = new formidable.IncomingForm();
        form.parse(req, async(err, fields, files) => {
            if (err) {
                this.reBody(res)
                return
            }
            const { pid, UserToken, is_menu, _id } = fields;
            //验证token
            let xtoken = await this.settoken(req, UserToken);
            if (xtoken == 1) {
                //1007表示要重新登录
                this.reBody(res, 0, 1007, "无效请求参数", '无效请求参数！')
                return
            }
            try {
                const adminData = await AdminModel.find({ _id: pid }, ['pid', 'Username']).populate('pid').exec()
                let menuData = []
                const userGroupmenuData = await userGroupMenuModel.findOne({ userGroup_id: adminData[0].pid._id })
                let z = 10000,
                    m = "";
                for (let i = 0; i < adminData[0].pid.length; i++) {
                    if (adminData[0].pid[i].levels < z) {
                        z = adminData[0].pid[i].levels
                        m = adminData[0].pid[i].Account
                    }
                }
                //如果是超级管理员查看
                if (m == "admin") {
                    menuData = await menuModel.find().populate('tubiao').exec()
                    for (let i = 0; i < menuData.length; i++) {
                        menuData[i] = menuData[i].toObject();
                    }
                } else {
                    //不是超级管理员
                    //通过权限找到所有拥有的权限详情
                    for (let i = 0; i < userGroupmenuData.menu_id.length; i++) {
                        menuData[i] = await menuModel.findOne({ _id: userGroupmenuData.menu_id[i] }).populate('tubiao').exec()
                        //转成普通对象
                        menuData[i] = menuData[i].toObject();
                    }
                }
                const c = await userGroupMenuModel.findOne({ userGroup_id: _id })
                //找菜单栏
                let a = this.revolveArr(menuData, is_menu)
                var b = {
                    treeData: a,
                    treeform: c.menu_id
                }
                this.reBody(res, 0, 1001, "无效请求参数", b)

            } catch (err) {
                this.reBody(res, 0, 1005, "无效请求参数", '无效请求参数！')
            }
        })
    }
    //更改权限
    async saveauth(req, res, next) {
        //通过该方法创建一个form表单
        const form = new formidable.IncomingForm();
        //提交的数据，cb为回调函数
        form.parse(req, async(err, fields, files) => {
            if (err) {
                this.reBody(res)
                return
            }
            const { pid, rules, UserToken } = fields;
            if (!pid) {
                this.reBody(res, 0, 1005, "无效请求参数", '无效请求参数！')
            }

            //验证token
            let xtoken = await this.settoken(req, UserToken);
            if (xtoken == 1) {
                //1007表示要重新登录
                this.reBody(res, 0, 1007, "无效请求参数", '无效请求参数！')
                return
            }
            try {
                //更改权限
                var a = await this.setPower(pid, rules, 'set')
                if (a) {
                    this.reBody(res, 0, 1001, "无效请求参数", '操作成功！')
                } else {
                    this.reBody(res, 0, 1005, "无效请求参数", '无效请求参数！')
                }
            } catch (err) {
                this.reBody(res, 0, 1005, "无效请求参数", '无效请求参数！')
            }
        })
    }
    //添加数据
    async insert(req, res, next) {
        //通过该方法创建一个form表单
        const form = new formidable.IncomingForm();
        //提交的数据，cb为回调函数
        form.parse(req, async(err, fields, files) => {
            if (err) {
                this.reBody(res)
                return
            }
            const { data, Account, UserToken } = fields;
            //验证token
            let xtoken = await this.settoken(req, UserToken);
            if (xtoken == 1) {
                //1007表示要重新登录
                this.reBody(res, 0, 1007, "无效请求参数", '无效请求参数！')
                return
            }
            try {
                let newData = {
                    status: data.status,
                    describe: data.describe,
                    Account: data.Account
                }
                //存入用户组表
                let a = await userGroupModel.create(newData)
                //存用户组关联表
                let b = await userGroupMenuModel.create({ userGroup_id: a._id, menu_id: [] })
                console.log(b)
                this.reBody(res, 0, 1001, "无效请求参数", '操作成功！')
            } catch (err) {
                this.reBody(res, 0, 1005, "无效请求参数", '无效请求参数！')
            }
        })
    }
    //更新数据
    async update(req, res, next) {
        //通过该方法创建一个form表单
        const form = new formidable.IncomingForm();
        //提交的数据，cb为回调函数
        form.parse(req, async(err, fields, files) => {
            if (err) {
                this.reBody(res)
                return
            }
            const { data, Account, UserToken } = fields;
            //验证token
            let xtoken = await this.settoken(req, UserToken);
            if (xtoken == 1) {
                //1007表示要重新登录
                this.reBody(res, 0, 1007, "无效请求参数", '无效请求参数！')
                return
            }
            try {
                let newData = {
                    status: data.status,
                    describe: data.describe,
                    Account: data.Account
                }
                await userGroupModel.update({ _id: data._id }, { $set: newData }, { upsert: true })

                this.reBody(res, 0, 1001, "无效请求参数", '操作成功！')
            } catch (err) {
                this.reBody(res, 0, 1005, "无效请求参数", '无效请求参数！')
            }
        })
    }
    //删除数据
    async deletes(req, res, next) {
        //通过该方法创建一个form表单
        const form = new formidable.IncomingForm();
        //提交的数据，cb为回调函数
        form.parse(req, async(err, fields, files) => {
            if (err) {
                this.reBody(res)
                return
            }
            const { _id, UserToken } = fields;
            //验证token
            let xtoken = await this.settoken(req, UserToken);
            if (xtoken == 1) {
                //1007表示要重新登录
                this.reBody(res, 0, 1007, "无效请求参数", '无效请求参数！')
                return
            }
            try {
                await userGroupModel.remove({ _id: _id });
                await userGroupMenuModel.remove({ userGroup_id: _id })
                this.reBody(res, 0, 1001, "无效请求参数", "删除成功")
            } catch (err) {
                this.reBody(res, 0, 1005, "无效请求参数", '无效请求参数！')
            }
        })
    }
}

module.exports = new userGroup()