'use strict';
//读取文件
const AddressComponent = require('../../prototype/addressComponent')
const AdminModel = require('../../models/admin/admin.js')
const userGroupmenuModel = require('../../models/menu/userGroup_menu.js')
const menuModel = require('../../models/menu/menu.js')
const formidable = require('formidable')
class Menu extends AddressComponent {
    constructor() {
        super()
        this.read = this.read.bind(this)
        this.treeselect = this.treeselect.bind(this)
        this.insert = this.insert.bind(this)
        this.update = this.update.bind(this)
        this.deletes = this.deletes.bind(this)
    }
    //导航菜单栏数据
    //只能看到自己权限能看到的
    async read(req, res, next) {
        //通过该方法创建一个form表单
        const form = new formidable.IncomingForm();
        form.parse(req, async(err, fields, files) => {
            if (err) {
                this.reBody(res)
                return
            }
            const { pid, UserToken, is_menu } = fields;
            //验证token
            let xtoken = await this.settoken(req, UserToken);
            if (xtoken == 1) {
                //1007表示要重新登录
                this.reBody(res, 0, 1007, "无效请求参数", '无效请求参数！')
                return
            }
            try {
                if (!pid) {
                    this.reBody(res, 0, 1005, "无效请求参数", '无效请求参数！')
                }
            } catch (err) {
                this.reBody(res)
                return
            }
            try {
                //通过ID找到身份
                const adminData = await AdminModel.find({ _id: pid }, ['pid', 'Username']).populate('pid').exec()
                //通过身份找到拥有的权限
                let menuData = []
                if (adminData[0].Username != 'admin') {
                    const userGroupmenuData = await userGroupmenuModel.findOne({ userGroup_id: adminData[0].pid._id })
                    //通过权限找到所有拥有的权限详情
                    for (let i = 0; i < userGroupmenuData.menu_id.length; i++) {
                        menuData[i] = await menuModel.findOne({ _id: userGroupmenuData.menu_id[i] }).populate('tubiao').exec()
                        //转成普通对象
                        menuData[i] = menuData[i].toObject();
                    }
                } else {
                    menuData = await menuModel.find().populate('tubiao').exec()
                    for (let i = 0; i < menuData.length; i++) {
                        menuData[i] = menuData[i].toObject();
                    }
                }
                //找菜单栏
                let a = this.revolveArr(menuData, is_menu)
                this.reBody(res, 0, 1001, "无效请求参数", a)
            } catch (err) {
                this.reBody(res, 0, 1005, "无效请求参数", '无效请求参数！')
            }
        })
    }
    // 获取所有不要权限
    async treeselect(req, res, next) {
        //通过该方法创建一个form表单
        const form = new formidable.IncomingForm();
        //提交的数据，cb为回调函数
        form.parse(req, async(err, fields, files) => {
            if (err) {
                this.reBody(res)
                return
            }
            try {
                //验证token
                const { is_menu, UserToken } = fields;
                let xtoken = await this.settoken(req, UserToken);
                if (xtoken == 1) {
                    //1007表示要重新登录
                    this.reBody(res, 0, 1007, "无效请求参数", '无效请求参数！')
                    return
                }
                //获取数据
                let menuData = await menuModel.find({}, ['api_name', 'is_menu', 'menu_name', 'router_name', 'top_id', 'treeid', 'tubiao']);
                for (var i = 0; i < menuData.length; i++) {
                    menuData[i] = menuData[i].toObject();
                }
                if (is_menu == "no") {
                    this.reBody(res, 0, 1001, "无效请求参数", menuData)
                } else {
                    //找所有
                    let a = this.revolveArr(menuData, -1)
                    this.reBody(res, 0, 1001, "无效请求参数", a)
                }
            } catch (err) {
                this.reBody(res, 0, 1005, "无效请求参数", '无效请求参数！2')
            }
        })
    }
    //添加数据
    async insert(req, res, next) {
        //通过该方法创建一个form表单
        const form = new formidable.IncomingForm();
        //提交的数据，cb为回调函数
        form.parse(req, async(err, fields, files) => {
            if (err) {
                this.reBody(res)
                return
            }
            const { top_id, tubiao, UserToken, router_name, menu_name, is_menu, api_name } = fields;
            //验证token
            let xtoken = await this.settoken(req, UserToken);
            if (xtoken == 1) {
                //1007表示要重新登录
                this.reBody(res, 0, 1007, "无效请求参数", '无效请求参数！')
                return
            }
            try {
                let newWordbook = {
                    api_name: api_name,
                    is_menu: is_menu,
                    menu_name: menu_name,
                    router_name: router_name,
                    top_id: top_id,
                    tubiao: tubiao,
                    update_time: Date.parse(new Date()).toString()
                }
                console.log(newWordbook)
                let menuData = await menuModel.create(newWordbook)
                this.reBody(res, 0, 1001, "无效请求参数", menuData)
            } catch (err) {
                this.reBody(res, 0, 1005, "无效请求参数", '无效请求参数！')
            }
        })
    }
    //更新数据
    async update(req, res, next) {
        //通过该方法创建一个form表单
        const form = new formidable.IncomingForm();
        //提交的数据，cb为回调函数
        form.parse(req, async(err, fields, files) => {
            if (err) {
                this.reBody(res)
                return
            }
            const { top_id, tubiao, UserToken, router_name, menu_name, is_menu, api_name, _id } = fields;
            //验证token
            let xtoken = await this.settoken(req, UserToken);
            if (xtoken == 1) {
                //1007表示要重新登录
                this.reBody(res, 0, 1007, "无效请求参数", '无效请求参数！')
                return
            }
            try {
                let newWordbook;
                newWordbook = {
                    api_name: api_name,
                    is_menu: is_menu,
                    menu_name: menu_name,
                    router_name: router_name,
                    top_id: top_id,
                    tubiao: tubiao,
                    update_time: Date.parse(new Date()).toString()
                }

                await menuModel.update({ _id: _id }, newWordbook)
                this.reBody(res, 0, 1001, "无效请求参数", '操作成功！')
            } catch (err) {
                this.reBody(res, 0, 1005, "无效请求参数", '无效请求参数！')
            }
        })
    }
    //删除数据
    async deletes(req, res, next) {
        //通过该方法创建一个form表单
        const form = new formidable.IncomingForm();
        //提交的数据，cb为回调函数
        form.parse(req, async(err, fields, files) => {
            if (err) {
                this.reBody(res)
                return
            }
            const { _id, UserToken } = fields;
            //验证token
            let xtoken = await this.settoken(req, UserToken);
            if (xtoken == 1) {
                //1007表示要重新登录
                this.reBody(res, 0, 1007, "无效请求参数", '无效请求参数！')
                return
            }
            try {
                //通过ID找到身份
                const WordbookData = await menuModel.find()
                let menuData = []
                //通过权限找到所有拥有的权限详情
                for (let i = 0; i < WordbookData.length; i++) {
                    //转成普通对象
                    menuData[i] = WordbookData[i].toObject();
                }
                //获取要删除的ID
                var a = this.getChildId(menuData, _id)
                console.log(a)
                for (let i in a) {
                    await menuModel.remove({ _id: a[i] });
                }
                this.reBody(res, 0, 1001, "无效请求参数", "删除成功")
            } catch (err) {
                this.reBody(res, 0, 1005, "无效请求参数", '无效请求参数！')
            }
        })
    }
}

module.exports = new Menu()