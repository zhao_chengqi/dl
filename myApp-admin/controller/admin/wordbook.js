'use strict';
//读取文件
const AddressComponent = require('../../prototype/addressComponent')
//用户表
const AdminModel = require('../../models/admin/admin.js')
//管理员权限
const userGroupmenuModel = require('../../models/menu/userGroup_menu.js')
//菜单
const menuModel = require('../../models/menu/menu.js')
//字典表
const WordbookModel = require('../../models/menu/wordbook.js')

const formidable = require('formidable')
class Wordbook extends AddressComponent {
    constructor() {
        super()
        this.index = this.index.bind(this)
        this.insert = this.insert.bind(this)
        this.update = this.update.bind(this)
        this.deletes = this.deletes.bind(this)

    }
    //导航菜单栏数据
    async index(req, res, next) {
        //通过该方法创建一个form表单
        const form = new formidable.IncomingForm();
        //提交的数据，cb为回调函数
        form.parse(req, async(err, fields, files) => {
            if (err) {
                this.reBody(res)
                return
            }
            const { is_tu, is_menu, UserToken } = fields;
            //验证token
            let xtoken = await this.settoken(req, UserToken);
            if (xtoken == 1) {
                //1007表示要重新登录
                this.reBody(res, 0, 1007, "无效请求参数", '无效请求参数！')
                return
            }
            try {
                //只获得菜单栏
                if (is_tu) {
                    const WordbookData = await WordbookModel.find()
                    var b = []
                    for (var i in WordbookData) {
                        if (WordbookData[i].top_id == WordbookData[0]._id) {
                            b.push(WordbookData[i])
                        }
                    }
                    this.reBody(res, 0, 1001, "无效请求参数", b)
                } else {
                    //通过ID找到身份
                    const WordbookData = await WordbookModel.find()
                    let menuData = []
                    //通过权限找到所有拥有的权限详情
                    for (let i = 0; i < WordbookData.length; i++) {
                        //转成普通对象
                        menuData[i] = WordbookData[i].toObject();
                    }
                    //不要分级
                    if (is_menu == 'no') {
                        this.reBody(res, 0, 1001, "无效请求参数", menuData)
                    } else {
                        //需要分级
                        let a = this.revolveArr(menuData, is_menu)
                        this.reBody(res, 0, 1001, "无效请求参数", a)
                    }
                }
            } catch (err) {
                this.reBody(res, 0, 1005, "无效请求参数", '无效请求参数！')
            }
        })
    }
    //添加数据
    async insert(req, res, next) {
        //通过该方法创建一个form表单
        const form = new formidable.IncomingForm();
        //提交的数据，cb为回调函数
        form.parse(req, async(err, fields, files) => {
            if (err) {
                this.reBody(res)
                return
            }
            const { liebie, selectedOptions, UserToken } = fields;
            //验证token
            let xtoken = await this.settoken(req, UserToken);
            if (xtoken == 1) {
                //1007表示要重新登录
                this.reBody(res, 0, 1007, "无效请求参数", '无效请求参数！')
                return
            }
            try {
                let newWordbook;
                if (selectedOptions.length == 0) {
                    //如果添加一级
                    //添加2级
                    newWordbook = {
                        top_id: "0",
                        bookname: liebie
                    }
                } else {
                    //添加2级
                    newWordbook = {
                        top_id: selectedOptions[selectedOptions.length - 1],
                        bookname: liebie
                    }
                }

                let WordbookData = await WordbookModel.create(newWordbook)
                this.reBody(res, 0, 1001, "无效请求参数", WordbookData)
            } catch (err) {
                this.reBody(res, 0, 1005, "无效请求参数", '无效请求参数！')
            }
        })
    }
    //更新数据
    async update(req, res, next) {
        //通过该方法创建一个form表单
        const form = new formidable.IncomingForm();
        //提交的数据，cb为回调函数
        form.parse(req, async(err, fields, files) => {
            if (err) {
                this.reBody(res)
                return
            }
            const { liebie, selectedOptions, UserToken } = fields;
            //验证token
            let xtoken = await this.settoken(req, UserToken);
            if (xtoken == 1) {
                //1007表示要重新登录
                this.reBody(res, 0, 1007, "无效请求参数", '无效请求参数！')
                return
            }
            try {
                let newWordbook;
                newWordbook = {
                    top_id: selectedOptions[selectedOptions.length - 1],
                    bookname: liebie
                }

                await WordbookModel.update({ _id: newWordbook.top_id }, { bookname: newWordbook.bookname })
                this.reBody(res, 0, 1001, "无效请求参数", '操作成功！')
            } catch (err) {
                this.reBody(res, 0, 1005, "无效请求参数", '无效请求参数！')
            }
        })
    }
    //删除数据
    async deletes(req, res, next) {
        //通过该方法创建一个form表单
        const form = new formidable.IncomingForm();
        //提交的数据，cb为回调函数
        form.parse(req, async(err, fields, files) => {
            if (err) {
                this.reBody(res)
                return
            }
            const { _id, UserToken } = fields;
            //验证token
            let xtoken = await this.settoken(req, UserToken);
            if (xtoken == 1) {
                //1007表示要重新登录
                this.reBody(res, 0, 1007, "无效请求参数", '无效请求参数！')
                return
            }
            try {
                //通过ID找到身份
                const WordbookData = await WordbookModel.find()
                let menuData = []
                //通过权限找到所有拥有的权限详情
                for (let i = 0; i < WordbookData.length; i++) {
                    //转成普通对象
                    menuData[i] = WordbookData[i].toObject();
                }
                var a = this.getChildId(menuData, _id)
                for (let i in a) {
                    await WordbookModel.remove({ _id: a[i] });
                }
                this.reBody(res, 0, 1001, "无效请求参数", "删除成功")
            } catch (err) {
                this.reBody(res, 0, 1005, "无效请求参数", '无效请求参数！')
            }
        })
    }

}


module.exports = new Wordbook()