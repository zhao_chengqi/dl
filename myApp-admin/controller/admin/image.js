'use strict';
//读取文件
const AddressComponent = require('../../prototype/addressComponent')
const http = require('http')
const fs = require('fs');
const config = require('config-lite')(__dirname);
var path = require('path');

class Image extends AddressComponent {
    constructor() {
        super()
        this.readImage = this.readImage.bind(this)
    }
    //读取图片
    readImage(req, res, next) {
        var filePath = path.join("e:/www/myAPP/myApp-admin/", req.originalUrl);
        console.log(filePath)
        //判断是否有这个图片
        fs.exists(filePath, function(exists) {
            res.sendfile(exists ? filePath : null);
        });
    }
}

module.exports = new Image()