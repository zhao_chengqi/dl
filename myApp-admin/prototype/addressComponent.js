'use strict';
//索引
const Ids = require('../models/ids')
//菜单表
const menu = require('../models/menu/menu')
//菜单表和用户组的关联表
const userGroup_menu = require('../models/menu/userGroup_menu')
//用户级别表
const userGroup = require('../models/admin/userGroup')
//用户表
const AdminModel = require('../models/admin/admin')
//字典表
const wordbookModel = require('../models/menu/wordbook')
//公共的
var isself;
class AddressComponent {
    constructor() {
        this.idList = ['admin_id', 'menu_id'];
        this.getId = this.getId.bind(this)
        this.setPower = this.setPower.bind(this)
    }
    //获取数据库索引id列表
    async getId(type) {
        //判断一个数组是否包含一个指定的值
        if (!this.idList.includes(type)) {
            console.log('id类型错误');
            throw new Error('id类型错误');
            return
        }
        try {
            const idData = await Ids.findOne();
            idData[type]++;
            await idData.save();
            return idData[type]
        } catch (err) {
            console.log('获取ID数据失败');
            throw new Error(err)
        }
    }
    //设定返回body数据的格式和内容
    reBody(res, count = 0, code = 1008, title = "服务器繁忙，请稍后再试", data = "服务器繁忙，请稍后再试") {
        res.send({
            count: 0,
            status: {
                code: code,
                title: title
            },
            data: data
        })
    }
    /**
     * [setPower 设置权限 注册进权限  改动权限]
     * @param {[type]} _id      [用户组id]
     * @param {Number} objectId [需要增加的菜单id]
     * @param {String} status   [是否是注测]
     */
    async setPower(_id, objectId, status = '') {
        //当菜单id==1说明是第一个用户所以要初始第一个的权限,status = 'zhuce'为注测进来
        if (objectId == 1 && status == 'zhuce') {
            //新增
            //菜单表再增加两条数据
            const menuData = await menu.find();
            var tubiao = await wordbookModel.findOne();
            await menu.create({
                top_id: menuData[0]._id,
                menu_name: "我的面板",
                tubiao: tubiao._id,
                router_name: 'home',
                api_name: '',
                is_menu: 1,
                update_time: Date.parse(new Date()).toString()
            })
            await menu.create({
                top_id: menuData[1]._id,
                menu_name: "导航菜单管理",
                tubiao: tubiao._id,
                router_name: 'navigation',
                api_name: '',
                is_menu: 1,
                update_time: Date.parse(new Date()).toString()
            })
            await menu.create({
                top_id: menuData[1]._id,
                menu_name: "字典表管理",
                tubiao: tubiao._id,
                router_name: 'category',
                api_name: '',
                is_menu: 1,
                update_time: Date.parse(new Date()).toString()
            })
            //获取菜单表的第一个
            const menuDatas = await menu.find();
            var menuDataArr = [];
            for (let i in menuDatas) {
                menuDataArr[i] = menuDatas[i]._id
            }
            //加入所有权限
            isself = await userGroup_menu.create({ userGroup_id: _id, menu_id: menuDataArr })
            if (isself) {
                return true;
            } else {
                return false;
            }
        } else if (status == 'set') {
            // //改动权限
            // //查询用户组表有没有
            const userGroupData = await userGroup.findOne({ _id: _id })
            if (!userGroupData) {
                return false;
            }
            if (objectId instanceof Array) {
                isself = await userGroup_menu.update({ userGroup_id: _id }, { $set: { menu_id: objectId } }, { upsert: true })
                if (isself) {
                    return true;
                } else {
                    return false;
                }
            }
        }
    }
    //无限级分类，把一维数组专多维数组
    /**
     * [revolveArr 无限及分类 从父级找所有]
     * @param  {[type]} arr  [原始数据]
     * @param  {[type]} c    [是获得菜单，还所有 ,-1所有，0不是菜单，1是菜单]
     * @return {[type]}      [返回多为数组]
     */

    revolveArr(arr, c) {
        var yuanArr = arr;
        var id1Arr = [],
            k = 0;
        for (var i = 0; i < arr.length; i++) {
            if (arr[i].top_id == 0) {
                id1Arr[k++] = arr[i]
            }
        }

        function getArr(Arr, n, c) {
            //循环自身
            for (var i = 0; i < Arr.length; i++) {
                n = 0;
                //找出自己的子集
                for (var j = 0; j < yuanArr.length; j++) {
                    //通过自己的id找出自己的子集id
                    if (Arr[i]._id == yuanArr[j].top_id) {
                        //如果是获得所有
                        if (c == -1) {
                            if (!Arr[i].children) {
                                Arr[i].children = [];
                            }
                            Arr[i].children[n] = yuanArr[j];
                            n++;
                            getArr(Arr[i].children, 0, c);
                        } else {
                            //获得摸一个
                            if (yuanArr[j].is_menu == c) {
                                if (!Arr[i].children) {
                                    Arr[i].children = [];
                                }
                                Arr[i].children[n] = yuanArr[j];
                                n++;
                                getArr(Arr[i].children, 0, c);
                            }
                        }
                    }
                }
            }
            return Arr
        }
        //找子集
        let xg = getArr(id1Arr, 0, c);
        return xg;
    }
    /**
     * [getChildId 通过id返回自己所有子集的ID]
     * @param  {[type]} menuData [原始数组]
     * @param  {[type]} _id      [最顶级ID]
     * @return {[type]}          [返回自己子集的所有ID]
     */
    getChildId(menuData, _id) {
        var idArr = [];
        idArr.push(_id)

        function getCid(_id) {
            for (var i = 0; i < menuData.length; i++) {
                //通过自己的ID找所有的子ID
                if (_id == menuData[i].top_id) {
                    idArr.push(menuData[i]._id)
                    getCid(menuData[i]._id)
                }
            }
        }
        getCid(_id)
        return idArr;
    }
    /**
     * [settoken 存session]
     * @param  {[type]} req       [请求]
     * @param  {[type]} UserToken [传的token]
     * @param  {[type]} key       [存的KEY]
     * @return {[type]}           [description]
     */
    async settoken(req, UserToken, key = 'userinfo') {
        if (req.session[key] == undefined) {
            req.session[key] = {}
        }
        //服务器没有数据所以要新建
        if (!req.session[key][UserToken]) {
            req.session[key][UserToken] = {}
        } else {
            //有数据表示可以继续别的操作
            return 0;
        }
        //新建数据前提，获得数据
        let adminData = await AdminModel.findOne({ token: UserToken })
        //数据库有token
        if (adminData) {
            //存入token
            req.session[key][UserToken] = adminData
            //有数据表示可以继续别的操作
            return 0;
        } else {
            //没有token表示过期或者被销毁需要重新下登录
            return 1;
        }
    }
}
module.exports = AddressComponent