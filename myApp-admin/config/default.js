'use strict';
require('express');
module.exports = {
    port: 7001,
    url: 'mongodb://localhost:27017/myApp',
    session: {
        name: 'SID', //这里的name值得是cookie的name，默认cookie的name是：connect.sid
        secret: 'SID',
        cookie: {
            httpOnly: true,
            secure: false,
            maxAge: 365 * 24 * 60 * 60 * 1000, //失效时间
        }
    }
}