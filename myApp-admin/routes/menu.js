'use strict';

const express = require('express')
const menu = require('../controller/admin/menu')
const router = express.Router()
//匹配路劲后的请求方式
router.post('/read', menu.read);
router.post('/insert', menu.insert);
router.post('/update', menu.update);
router.post('/deletes', menu.deletes);
router.post('/treeselect', menu.treeselect);
module.exports = router