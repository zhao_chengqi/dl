'use strict';

const express = require('express')
const image = require('../controller/admin/image')
const router = express.Router()
//匹配路劲后的请求方式
router.get('/image/*', image.readImage);
module.exports = router