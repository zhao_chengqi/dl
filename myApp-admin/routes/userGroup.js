'use strict';

const express = require('express')
const userGroup = require('../controller/userGroup/userGroup.js')
const router = express.Router()
//匹配路劲后的请求方式
router.post('/index', userGroup.index);
router.post('/read', userGroup.read);
router.post('/insert', userGroup.insert);
router.post('/update', userGroup.update);
router.post('/saveauth', userGroup.saveauth);
router.post('/deletes', userGroup.deletes);
module.exports = router