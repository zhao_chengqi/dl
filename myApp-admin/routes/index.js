'use strict';
//登录注册
const admin = require('./admin')
//读取图片
const image = require('./image')
//导航菜单
const menu = require('./menu')
//字典表
const wordbook = require('./wordbook')
//用户组管理
const userGroup = require('./userGroup')
module.exports = (app) => {
    // app.get('/', (req, res, next) => {
    //  res.redirect('/');
    // });
    //当挂载当前路由时调用
    app.use('/admin', admin);
    //读取文件
    app.use('/disc', image)
    //导航菜单
    app.use('/menu', menu)
    //字典表
    app.use('/wordbook', wordbook)
    //用户组管理
    app.use('/userGroup', userGroup)
}