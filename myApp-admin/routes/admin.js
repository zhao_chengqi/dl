'use strict';

const express = require('express')
const Admin = require('../controller/admin/admin')
const router = express.Router()
//匹配路劲后的请求方式
router.post('/login', Admin.login);
router.post('/index', Admin.index);
router.post('/register', Admin.register);
module.exports = router