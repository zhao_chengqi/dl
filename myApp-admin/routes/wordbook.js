'use strict';
//字典表
const express = require('express')
const wordbook = require('../controller/admin/wordbook')
const router = express.Router()
//匹配路劲后的请求方式
router.post('/index', wordbook.index);
router.post('/update', wordbook.update);
router.post('/deletes', wordbook.deletes);
router.post('/insert', wordbook.insert);
module.exports = router