'use strict';
//管理员表
const config = require('config-lite')(__dirname);
const mongoose = require('mongoose')
// 一种以文件形式存储的数据库模型骨架，不具备数据库的操作能力
const Schema = mongoose.Schema;
//用户组
const userGroup = require('./userGroup.js');
//定义一个schema
const adminSchema = new Schema({
    Username: String,
    name: { type: String, default: "" },
    phone: { type: String, default: "" },
    email: { type: String, default: "" },
    Password: String,
    id: { type: Number, default: 0 },
    status: { type: Number, default: 1 },
    //外键
    pid: [{ type: Schema.Types.ObjectId, ref: 'userGroup' }],
    img: { type: String, default: '/disc/image/Headimg/profile-photos/1.png' },
    create_time: { type: String, default: Date.parse(new Date()).toString() },
    update_time: { type: String, default: Date.parse(new Date()).toString() },
    token: { type: String, default: '' }
})
//建立索引提高查询速度
adminSchema.index({ id: 1 });
//由Schema发布生成的模型，具有抽象属性和行为的数据库操作对
const Admin = mongoose.model('Admin', adminSchema);

module.exports = Admin