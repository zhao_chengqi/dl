'use strict';
//用户组表
const mongoose = require('mongoose')
// 一种以文件形式存储的数据库模型骨架，不具备数据库的操作能力
const Schema = mongoose.Schema;
//菜单栏
//定义一个schema
const userGroupSchema = new Schema({
    Account: String,
    describe: String,
    levels: { type: Number, default: 9999 },
    status: { type: Number, default: 1 }
})
const userGroup = mongoose.model('userGroup', userGroupSchema);
userGroup.findOne((err, data) => {
    if (!data) {
        const userGroupData = new userGroup({
            Account: "超级管理员",
            describe: '拥有所有权限',
            levels: 0,
            status: 1
        });
        userGroupData.save();
    }
})
module.exports = userGroup