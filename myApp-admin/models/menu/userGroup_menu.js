'use strict'
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const menu = require('./menu.js');
const userGroup = require('../admin/userGroup.js');

const userGroup_menuSchema = new Schema({
    userGroup_id: { type: Schema.Types.ObjectId, ref: 'userGroup' },
    menu_id: [{ type: Schema.Types.ObjectId, ref: 'menu' }]
});
const userGroup_menu = mongoose.model('userGroupmenu', userGroup_menuSchema);

module.exports = userGroup_menu