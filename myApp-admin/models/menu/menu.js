'use strict';
//菜单栏
const mongoose = require('mongoose')
// 一种以文件形式存储的数据库模型骨架，不具备数据库的操作能力
const Schema = mongoose.Schema;
// const idsModel = require('../ids.js');
const wordbookModel = require('./wordbook')
//定义一个schema
const menuSchema = new Schema({
    top_id: { type: String, default: '0' },
    menu_name: { type: String, default: '' },
    tubiao: { type: Schema.Types.ObjectId, ref: 'wordbook' },
    router_name: { type: String, default: '' },
    api_name: { type: String },
    treeid: { type: Number, default: 0 },
    is_menu: { type: Number, default: 0 },
    create_time: { type: String, default: Date.parse(new Date()).toString() },
    update_time: { type: String, default: Date.parse(new Date()).toString() },

})
//由Schema发布生成的模型，具有抽象属性和行为的数据库操作对
const Menu = mongoose.model('Menu', menuSchema);
Menu.findOne((err, data) => {
    if (!data) {
        wordbookModel.findOne({}, function(err, data1) {
            //1为导航菜单 0不是盗汗菜单
            var MenuData = new Menu({
                top_id: '0',
                menu_name: "基本功能",
                tubiao: 'ti-menu-alt',
                router_name: '',
                api_name: '',
                tubiao: data1._id,
                is_menu: 1,
                update_time: Date.parse(new Date()).toString()
            })
            MenuData.save()
            var MenuData = new Menu({
                top_id: '0',
                menu_name: "系统设置",
                tubiao: 'ti-menu-alt',
                router_name: '',
                api_name: '',
                tubiao: data1._id,
                is_menu: 1,
                update_time: Date.parse(new Date()).toString()
            })
            MenuData.save()
        })
    }
})
module.exports = Menu