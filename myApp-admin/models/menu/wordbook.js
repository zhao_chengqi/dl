'use strict';
//用户组表
const mongoose = require('mongoose')
// 一种以文件形式存储的数据库模型骨架，不具备数据库的操作能力
const Schema = mongoose.Schema;
//菜单栏
//定义一个schema
const wordbookSchema = new Schema({
    top_id: String,
    bookname: String
})
const wordbook = mongoose.model('wordbook', wordbookSchema);
wordbook.findOne((err, data) => {
    if (!data) {
        const wordbookData = new wordbook({
            top_id: "0",
            bookname: "菜单图标"
        });
        wordbookData.save();
    }
})
module.exports = wordbook;