const express = require('express');
require('./mongodb/db.js');
//在进程目录下寻找 config 文件夹, 然后再引用 default。
const config = require('config-lite')(__dirname);
const router = require('./routes/index.js');
const path = require('path');
//通过指定索引页对代理请求的中间件，对于使用HTML5历史API的单页应用程序非常有用。
// const history = require('connect-history-api-fallback');

//该模块用于将session存入mongo中
const connectMongo = require('connect-mongo');
//加载用于解析 cookie 的中间件
const cookieParser = require('cookie-parser')
const session = require('express-session');
const app = express();

app.all('*', (req, res, next) => {
    res.header("Access-Control-Allow-Origin", req.headers.origin);
    res.header("Access-Control-Allow-Headers", "Content-Type, Authorization, X-Requested-With");
    res.header("Access-Control-Allow-Methods", "PUT,POST,GET,DELETE,OPTIONS");
    res.header("Access-Control-Allow-Credentials", true); //可以带cookies
    res.header("X-Powered-By", '3.2.1')
    if (req.method == 'OPTIONS') {
        res.send(200);
    } else {
        next();
    }
});
//加载用于解析 cookie 的中间件
app.use(cookieParser());
//存入mongo服务器
const MongoStore = connectMongo(session);
//存储到内存中
app.use(session({
    //返回客户端的key的名称，默认为connect.sid,也可以自己设置。
    name: config.session.name,
    //:一个String类型的字符串，作为服务器端生成session的签名
    secret: config.session.secret,
    //:(是否允许)当客户端并行发送多个请求时，其中一个请求在另一个请求结束时对session进行修改覆盖并保存。
    resave: true,
    //:初始化session时是否保存到存储
    saveUninitialized: false,
    //设置返回到前端key的属性，默认值为{ path: '/', httpOnly: true, secure: false, maxAge: null }
    cookie: config.session.cookie,
    //保存session的地方
    store: new MongoStore({
        //通过路径链接数据库
        url: config.url
    })
}));
router(app);

//浏览器就默认是请求的是服务器端的路由，有可能会报错所以使用这个中间件
// // 代理请求返回一个指定的页面的中间件
// app.use(history());
// //设置静态文件目录
// app.use((err, req, res, next) => {
//     res.status(404).send('未找到当前路由');
// });


app.listen(config.port);