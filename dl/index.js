var http = require('http'),
    httpProxy = require('http-proxy'),
    proxy = httpProxy.createProxyServer({});
proxy.on('error', function(err, req, res) {
    res.writeHead(500, {
        'Content-Type': 'text/plain'
    });
    res.end('Something went wrong.');
});
var server = require('http').createServer(function(req, res) {
    var host = req.headers.host;
    console.log(host)
    switch (host) {
        case 'guwanm.haiousystem.com':
            proxy.web(req, res, {
                target: 'http://127.0.0.1:8001'
            });
            break;
        case 'api.guwan.tianbaoweipai.com':
            proxy.web(req, res, {
                target: 'http://127.0.0.1:3000'
            });
            break;
        default:
            proxy.web(req, res, {
                target: 'http://127.0.0.1:3000'
            });
    }
});
server.listen(80);
