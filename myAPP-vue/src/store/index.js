import Vue from 'vue'
import Vuex from 'vuex'

//导入LocalStorage的封装器，详细使用方式参考 Git上storejs
import LS from 'storejs'

Vue.use(Vuex)

//定义数据仓库前缀，避免保存到LocalStorage中的数据节点冲突
var sysKeys = 'DB_MY_APP_'

/**
 * 用户偏好设置
 * @type {Object}
 * layout 界面布局设置
 * slider 侧边栏设置
 * skin 界面皮肤设置
 * bgImg 登录页面背景图片
 */
const settings = {
  layout: {
    boxLayout: {
      status: false,
      bgimg: ''
    },
    animate: {
      status: true,
      type: 'effect'
    },
    fixed: {
      header: false,
      footer: false,
      slider: false
    }
  },
  slider: {
    userinfo: true,
    fastlink: true,
    menu: false
  },
  skin: 'static/css/themes/type-b/theme-ocean.min.css',
  bgImg: ''
}
/**
 * [userinfo description]
 * @type {Object}
 */
const userinfo = { userinfo: {}, rules: [] }

/**
 * [nav_menu 用户左侧导航菜单]
 * @type {Array}
 */
const nav_menu = []

/**
 * [API_Err_Info 接口访问失败返回数据]
 * @type {Object}
 */
const API_Err_Info = {
  title: '啊哦，出错咯！',
  message: '接口请求失败，赶紧联系工程师抢修吧~',
  duration: 0,
  offset: 5
}

/**
 * [rulesArr 存储的角色组]
 * @type {Array}
 */
const rulesArr = []

/**
 * [state description]
 * @type {Object}
 */
const state = {
  settings: settings,
  userinfo: userinfo,
  nav_menu: nav_menu,
  API_Err_Info: API_Err_Info,
  rulesArr: rulesArr
}


/* 从本地存储读取数据 */
for (var item in state) {
  LS.get(sysKeys + item) ? state[item] = JSON.parse(LS.get(sysKeys + item)) : false;
}


//从组件中提交请求方法，写在此类下，注意保存时，一定要保存为json
const mutations = {
  //设置界面是否为盒子布局
  setBoxLayout(state, data) {
    state.settings.layout.boxLayout.status = data
    LS.set(sysKeys + 'settings', JSON.stringify(state.settings));
  },
  //设置盒子背景图片
  setBoxBgimg(state, data) {
    state.settings.layout.boxLayout.bgimg = data
    LS.set(sysKeys + 'settings', JSON.stringify(state.settings));
  },
  //设置是否开启页面动画
  setAnimate(state, data) {
    state.settings.layout.animate = data
    LS.set(sysKeys + 'settings', JSON.stringify(state.settings));
  },
  //设置固定位置
  setFixed(state, payload) {
    state.settings.layout.fixed = payload
    LS.set(sysKeys + 'settings', JSON.stringify(state.settings));
  },
  //设置显示左侧信息
  setSlider(state, payload) {
    state.settings.slider = payload
    LS.set(sysKeys + 'settings', JSON.stringify(state.settings));
  },
  //设置皮肤
  setTheme(state, payload) {
    state.settings.skin = payload
    LS.set(sysKeys + 'settings', JSON.stringify(state.settings));
  },
  //设置登录页面背景图片
  setBgImg(state, payload) {
    state.settings.bgImg = payload
    LS.set(sysKeys + 'settings', JSON.stringify(state.settings));
  },
  //保存用户信息
  setUserinfo(state, payload) {
    state.userinfo = payload
    LS.set(sysKeys + 'userinfo', JSON.stringify(state.userinfo));
  },
  //存储需要显示菜单栏的菜单
  setNavMenu(state, payload) {
    state.nav_menu = payload
    LS.set(sysKeys + 'nav_menu', JSON.stringify(state.nav_menu));
  },
  //存储角色组
  setRulesArr(state, payload) {
    state.rulesArr = payload
    LS.set(sysKeys + 'rulesArr', JSON.stringify(state.rulesArr));
  }
}


//数据过滤
const getters = {
  Request_Head: state => {
    return {
      "UserToken": state.userinfo.userinfo.token
    }
  }
}
export default new Vuex.Store({
  state,
  mutations,
  getters
})
