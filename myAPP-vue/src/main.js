// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store/index.js'

import Element from 'element-ui'
import Bootstrap from 'bootstrap'
//请求接口
import fetch from '@/config/fetch.js'
import 'element-ui/lib/theme-default/index.css'
//获取代理
import { baseUrl } from '@/config/env'
Vue.use(Element)
//设置为 false 以阻止 vue 在启动时生成生产提示。
Vue.prototype._PageHeight = document.documentElement.clientHeight - 270
Vue.prototype.layout = false
Vue.config.productionTip = false
Vue.prototype.fetch = fetch
Vue.prototype.baseUrl = baseUrl
//成员管理的id
Vue.prototype.group = { _id: '' }
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App }
})
